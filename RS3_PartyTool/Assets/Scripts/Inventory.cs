﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    public List<Character> characters = new List<Character>();
    public CharacterDatabase characterDatabase;

    private void Start()
    {
        AddCharacter("Julian");
        AddCharacter("Leonid");
        AddCharacter("Monica");
        RemoveCharacter("Julian");
        RemoveCharacter("Ward");
    }

    public void AddCharacter(string name)
    {
        Character characterToAdd = characterDatabase.GetCharacter(name);
        characters.Add(characterToAdd);
        Debug.Log("Added character: " + characterToAdd.name);
    }

    public Character CheckForCharacter(string name)
    {
        return characters.Find(character => character.name == name);
    }

    public void RemoveCharacter(string name)
    {
        Character character = CheckForCharacter(name);
        if(character != null)
        {
            characters.Remove(character);
            Debug.Log("Character removed: " + character.name);
        }
    }
}
