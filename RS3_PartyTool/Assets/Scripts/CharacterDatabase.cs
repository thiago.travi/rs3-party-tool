﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CharacterDatabase : MonoBehaviour
{
    public List<Character> characters = new List<Character>();

    void BuildDatabase()
    {
        characters = Resources.LoadAll<Character>("Characters").ToList();
    }
    // Start is called before the first frame update
    void Awake()
    {
        BuildDatabase();
    }

    public Character GetCharacter(string name)
    {
        return characters.Find(character => character.name == name);
    }
}
