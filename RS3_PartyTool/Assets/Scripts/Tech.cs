﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Enums;

[CreateAssetMenu(fileName = "New Tech", menuName = "Tech", order = 52)]
public class Tech : ScriptableObject
{
    [SerializeField]
    public int number;
    [SerializeField]
    public string name;
    [SerializeField]
    public LearningTypes learningTypes;
    public Tech dependency;
    [SerializeField]
    public bool weaponExclusive;
    [SerializeField]
    public string weaponNeed;
}
