﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Enums
{
    public enum WeaponTypes
    {
        Sword,
        BigSword,
        Epee,
        Spear,
        Axe,
        Club,
        Bow,
        MartialArts
    }

    public enum MagicTypes
    {
        None,
        Fire,
        Water,
        Wind,
        Earth,
        Sun,
        Moon,
        Blood
    }
    [Flags]
    public enum LearningTypes
    {
        Type00 = (1 << 0),
        Type01 = (1 << 1),
        Type02 = (1 << 2),
        Type03 = (1 << 3),
        Type04 = (1 << 4),
        Type05 = (1 << 5),
        Type06 = (1 << 6),
        Type07 = (1 << 7),
        Type08 = (1 << 8),
        Type09 = (1 << 9),
        Type10 = (1 << 10),
        Type11 = (1 << 11)
    }
}