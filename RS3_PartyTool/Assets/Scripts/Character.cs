﻿using UnityEngine;
using static Enums;

[CreateAssetMenu(fileName = "New Character", menuName = "Character", order = 51)]
public class Character : ScriptableObject
{
    [SerializeField]
    string name;
    [SerializeField]
    Texture2D sprite;
    [SerializeField]
    public int strength;
    [SerializeField]
    public int dexterity;
    [SerializeField]
    public int speed;
    [SerializeField]
    public int constitution;
    [SerializeField]
    public int inteligence;
    [SerializeField]
    public int will;
    [SerializeField]
    public int charm;
    [HideInInspector]
    public int overall;
    [SerializeField]
    public int lp;
    [SerializeField]
    LearningTypes baseLearningType;
    [SerializeField]
    LearningTypes specialLearningType1;
    [SerializeField]
    WeaponTypes weaponNeededSpecial1;
    [SerializeField]
    LearningTypes specialLearningType2;
    [SerializeField]
    WeaponTypes weaponNeededSpecial2;
    [SerializeField]
    MagicTypes[] magicSpecializations;
}