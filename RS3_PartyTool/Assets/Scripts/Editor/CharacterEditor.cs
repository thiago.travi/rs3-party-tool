﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(Character))]
[CanEditMultipleObjects]
public class CharacterEditor : Editor
{
    SerializedProperty overall;

    Character myTarget;

    void OnEnable()
    {
        overall = serializedObject.FindProperty("overall");
    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        myTarget = (Character)target;

        myTarget.overall = myTarget.strength + myTarget.dexterity + myTarget.speed + myTarget.constitution + myTarget.inteligence + myTarget.will + myTarget.charm;
        EditorGUILayout.LabelField("Overall", myTarget.overall.ToString());
    }
}
